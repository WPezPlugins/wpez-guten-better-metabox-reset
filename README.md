## WPezGutenBetter Metabox Reset

__When using Gutenberg: having trouble moving metaboxes from below the content to the sidebar, or vice versa? This plugin will help you resolve this.__


### OVERVIEW

Fixes this...

- https://github.com/WordPress/gutenberg/issues/7960

By effectively doing this...

- https://github.com/WordPress/gutenberg/issues/7960#issuecomment-731867284


But then there's this...

- https://core.trac.wordpress.org/ticket/52818


-----

This plugin added a section to the User Profile page that allows the usermeta: meta-box-order_post to be reset. Just check the checkbox and save. That's it. 

Note: The default is to only show this new User Profile section to users with the capability of 'create_users'. Typically, this is the role of admin. However,  there's a filter that allows you to change the default capability. 
