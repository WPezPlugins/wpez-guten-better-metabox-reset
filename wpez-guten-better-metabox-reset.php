<?php
/**
 * Plugin Name: WPezPlugins: Guten Better - Metabox Reset
 * Plugin URI: https://gitlab.com/WPezPlugins/wpez-guten-better-metabox-reset
 * Description: Resets the usermeta: meta-box-order_post. For more details: https://github.com/WordPress/gutenberg/issues/7960
 * Version: 0.0.1
 * Author: MF Simchock (Chief Executive Alchemist) for Alchemy United
 * Author URI: https://AlchemyUnited.com
 * License: GPLv2 or later
 * Text Domain: wpez_gbmr
 *
 * @package WPezGutenBetterMetaboxReset
 */

namespace WPezGutenBetterMetaboxReset;

// register_deactivation_hook( __FILE__, __NAMESPACE__ . '\uninstall'  );
\register_uninstall_hook( __FILE__, __NAMESPACE__ . '\uninstall' );

/**
 * When the plugin is uninstalled, we need to do some cleanup.
 *
 * @return void
 */
function uninstall() {

	$str_last_reset = trim( pluginConfig( 'last_reset_slug' ) );

	global $wpdb;
	$sql = $wpdb->prepare( "DELETE FROM {$wpdb->prefix}usermeta WHERE meta_key = %s", $str_last_reset );
	$results = $wpdb->get_results( $sql );
}

/**
 * Various values that are needed / shared across the plugin.
 *
 * @param string $key The key to the array to return.
 *
 * @return string
 */
function pluginConfig( $key = '' ) {

	$slug = 'wpez_guten_better_metabox_reset';

	$arr_config = array(
		'current_user_can' => 'create_users',
		'plugin_slug'      => $slug,
		'check_slug'       => $slug . '_check',
		'last_reset_slug'  => $slug . '_last_reset',
	);

	if ( isset( $arr_config[ $key ] ) ) {
		return $arr_config[ $key ];
	}
	return null;
}

\add_action( 'show_user_profile', __NAMESPACE__ . '\userProfileFieldAdd' );
\add_action( 'edit_user_profile', __NAMESPACE__ . '\userProfileFieldAdd' );

/**
 * Adds our checkbox to the user profile page. Also displays the last reset date+time.
 *
 * @param object $user Instance of the WP user class for the current user.
 *
 * @return void
 */
function userProfileFieldAdd( $user ) {

	// The default cap is "admin level". This means that while this field is added to the user profile page,
	// only an admin will see it and can do the reset. Use this filter if you want user to be able to self-reset.
	$current_user_can = \apply_filters( __NAMESPACE__ . '/current_user_can', trim( pluginConfig( 'current_user_can' ) ) );
	if ( ! \current_user_can( $current_user_can ) ) {
		return;
	}
	$str_check      = trim( pluginConfig( 'check_slug' ) );
	$str_last_reset = trim( pluginConfig( 'last_reset_slug' ) );
	?>
	<h3><?php echo \esc_html( __( 'WPezGutenBetter', 'wpez_gbmr' ) ); ?></h3>

	<table class="form-table">
		<tr class="show-admin-bar user-admin-bar-front-wrap">
		<th scope="row"><?php echo \esc_html( __( 'Gutenberg Metabox Reset', 'wpez_gbmr' ) ); ?></th>
		<td>
			<label for="<?php echo \esc_attr( $str_check ); ?>">
				<input name="<?php echo \esc_attr( $str_check ); ?>" type="checkbox" id="<?php echo \esc_attr( $str_check ); ?>" value="1"
				<?php
				echo '>';
				echo \esc_attr( __( 'Check and then save to reset usermeta: meta-box-order_post.', 'wpez_gbmr' ) );
				$str_last_update = \get_user_meta( $user->ID, $str_last_reset, true );
				if ( ! empty( $str_last_update ) ) {

					echo ' (';
					echo \esc_attr( __( 'Last reset:', 'wpez_gbmr' ) );

					$date_format            = \get_option( 'date_format' );
					$time_format            = \get_option( 'time_format' );
					$str_last_update_offset = $str_last_update + \get_option( 'gmt_offset' ) * 60 * 60;

					echo ' ' . \esc_attr( date_i18n( $date_format, $str_last_update_offset ) );
					echo ' ' . \esc_attr( date_i18n( $time_format, $str_last_update_offset ) );
					echo ')';
				}
				?>
				</label><br>
		</td>
	</tr>
	</table>
	<?php
}

\add_action( 'personal_options_update', __NAMESPACE__ . '\userProfileFieldSave' );
\add_action( 'edit_user_profile_update', __NAMESPACE__ . '\userProfileFieldSave' );

/**
 * Was the checkbox we added checked? If so then act accordingly.
 *
 * @param int $user_id The current WP user's user ID.
 *
 * @return void
 */
function userProfileFieldSave( $user_id ) {

	if ( ! \current_user_can( 'edit_user', $user_id ) ) {
		return;
	}

	if ( empty( $_POST['_wpnonce'] ) || ! \wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
		return;
	}

	$str_check      = trim( pluginConfig( 'check_slug' ) );
	$str_last_reset = trim( pluginConfig( 'last_reset_slug' ) );

	// The check isn't saved, but the time() is.
	if ( ! empty( $_POST[ $str_check ] ) ) {

		$delete_user_meta = \delete_user_meta( $user_id, 'meta-box-order_post' );
		if ( true === $delete_user_meta ) {
			// When the delete_user_mate is successful then save the time().
			$bool_updated = \update_user_meta( $user_id, $str_last_reset, time() );
		}
	} 
}
